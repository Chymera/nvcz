import matplotlib.pyplot as plt
from os import path
from samri.plotting import summary, timeseries
from samri.utilities import bids_substitution_iterator
from samri.fetch.local import roi_from_atlaslabel
from samri.pipelines.utils import bids_data_selection

scratch_dir = path.expanduser('~/.scratch/nvcz')
preprocessed_dir = path.join(scratch_dir, 'preprocessed/')
data_selection = bids_data_selection(preprocessed_dir, structural_match=False, functional_match={'acquisition':['geEPI']}, subjects=False, sessions=False)
data_selection = data_selection.loc[data_selection['task']!='rest']

substitutions = data_selection.to_dict('records')

my_roi = 'data/rhp.nii.gz'
timecourses, designs, _, events_dfs, subplot_titles = summary.ts_overviews(substitutions, my_roi,
	ts_file_template="{}/l1_neurons/sub-{{subject}}/ses-{{session}}/sub-{{subject}}_ses-{{session}}_task-{{task}}_acq-geEPI_run-{{run}}_{{suffix}}.nii.gz".format(scratch_dir),
	betas_file_template="{}/l1_neurons/sub-{{subject}}/ses-{{session}}/sub-{{subject}}_ses-{{session}}_task-{{task}}_acq-geEPI_run-{{run}}_desc-betas_{{suffix}}.nii.gz".format(scratch_dir),
	design_file_template="{}/l1_neurons/sub-{{subject}}/ses-{{session}}/sub-{{subject}}_ses-{{session}}_task-{{task}}_acq-geEPI_run-{{run}}_desc-design_{{suffix}}.mat".format(scratch_dir),
	event_file_template='{}/preprocessed/sub-{{subject}}/ses-{{session}}/func/sub-{{subject}}_ses-{{session}}_task-{{task}}_acq-geEPI_run-{{run}}_events.tsv'.format(scratch_dir),
	n_jobs_percentage=0.3,
	scale_design=True,
	subplot_title_template='{subject} | {session} | {task} | {run}',
	)

timeseries.multi(timecourses, designs, events_dfs, subplot_titles,
	quantitative=False,
	ax_size=[1.7,0.73],
	ncols=4,
	model_line_multiplier=1,
	)

