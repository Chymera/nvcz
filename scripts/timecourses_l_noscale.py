import matplotlib.pyplot as plt
from os import path
from samri.plotting import summary, timeseries
from samri.utilities import bids_substitution_iterator
from samri.fetch.local import roi_from_atlaslabel

substitutions = bids_substitution_iterator(
	sessions=[
		'ketxyl',
		],
	subjects=[
		'7472',
		'7475',
		'7476',
		'7477',
		'7482',
		'7483',
		],
	runs=[
		'0',
		],
	modalities=[
		'bold',
		],
	data_dir='~/.scratch/nvcz',
	# BOLD scans are not recognized, since the current (=sci-biology/samri_bidsdata-0.2) filt file suffix also contains `_maths_` for CBV, but not for BOLD.
	validate_for_template="{data_dir}/l1_lr/sub-{subject}/ses-{session}/sub-{subject}_ses-{session}_task-lrhp_acq-geEPI_run-{run}_{modality}.nii.gz",
	)

my_roi = 'data/lhp.nii.gz'
timecourses, designs, _, events_dfs, subplot_titles = summary.ts_overviews(substitutions, my_roi,
	ts_file_template="{data_dir}/l1_lr/sub-{subject}/ses-{session}/sub-{subject}_ses-{session}_task-lrhp_acq-geEPI_run-{run}_{modality}.nii.gz",
	betas_file_template="{data_dir}/l1_lr/sub-{subject}/ses-{session}/sub-{subject}_ses-{session}_task-lrhp_acq-geEPI_run-{run}_desc-betas_{modality}.nii.gz",
	design_file_template="{data_dir}/l1_lr/sub-{subject}/ses-{session}/sub-{subject}_ses-{session}_task-lrhp_acq-geEPI_run-{run}_desc-design_{modality}.mat",
	event_file_template='{data_dir}/preprocessed/sub-{subject}/ses-{session}/func/sub-{subject}_ses-{session}_task-lrhp_acq-geEPI_run-{run}_events.tsv',
	n_jobs_percentage=0.3,
	scale_design=False,
	subplot_title_template='{subject} | {session} | {task} | {run}',
	)

# Dropping columns from list of Pandas DataFrames
designs = [df.drop(df.columns[[2,3,4]], axis=1) for df in designs]

timeseries.multi(timecourses, designs, events_dfs, subplot_titles,
	quantitative=False,
	ax_size=[3.4,1.2],
	)
