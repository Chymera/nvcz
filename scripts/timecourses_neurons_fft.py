import matplotlib.pyplot as plt
import numpy as np
from os import path
from samri.plotting import summary, timeseries
from samri.utilities import bids_substitution_iterator
from samri.fetch.local import roi_from_atlaslabel
from scipy.fftpack import fft
from samri.pipelines.utils import bids_data_selection

scratch_dir = path.expanduser('~/.scratch/nvcz')
preprocessed_dir = path.join(scratch_dir, 'preprocessed/')
data_selection = bids_data_selection(preprocessed_dir, structural_match=False, functional_match={'acquisition':['geEPI']}, subjects=False, sessions=False)

substitutions = data_selection.to_dict('records')

my_roi = 'data/rhp.nii.gz'
timecourses, designs, _, events_dfs, subplot_titles = summary.ts_overviews(substitutions, my_roi,
	ts_file_template="{}/l1_neurons/sub-{{subject}}/ses-{{session}}/sub-{{subject}}_ses-{{session}}_task-{{task}}_acq-geEPI_run-{{run}}_{{suffix}}.nii.gz".format(scratch_dir),
	betas_file_template="{}/l1_neurons/sub-{{subject}}/ses-{{session}}/sub-{{subject}}_ses-{{session}}_task-{{task}}_acq-geEPI_run-{{run}}_desc-betas_{{suffix}}.nii.gz".format(scratch_dir),
	design_file_template="{}/l1_neurons/sub-{{subject}}/ses-{{session}}/sub-{{subject}}_ses-{{session}}_task-{{task}}_acq-geEPI_run-{{run}}_desc-design_{{suffix}}.mat".format(scratch_dir),
	event_file_template='{}/preprocessed/sub-{{subject}}/ses-{{session}}/func/sub-{{subject}}_ses-{{session}}_task-{{task}}_acq-geEPI_run-{{run}}_events.tsv'.format(scratch_dir),
	n_jobs_percentage=0.3,
	scale_design=False,
	subplot_title_template='{subject} | {session} | {task} | {run}',
	)

rate=1
designs_fft = []
events_ffts = []
for design, events_df in zip(designs, events_dfs):
	design_fft = 20*np.log10(np.abs(np.fft.rfft(design[0])))
	f = np.linspace(0, rate/2, len(design_fft))
	designs_fft.append(design_fft)
	events = np.zeros(len(design))
	for d, o in zip(events_df["duration"], events_df["onset"]):
		d = round(d)
		o = round(o)
		events[o:o+d] = 1
	events_fft = 20*np.log10(np.abs(np.fft.rfft(events)))
	f = np.linspace(0, rate/2, len(events_fft))
	events_ffts.append(events_fft)

timeseries.multi(designs_fft, events_ffts, [], subplot_titles,
	quantitative=False,
	ax_size=[3.4,1.2],
	scale_x=len(events)
	)
