import seaborn as sns
import pandas as pd
import numpy as np
import ast
#generic = lambda x: ast.literal_eval(x)
#conv = {'BOLD [S.A.U.]': generic}

#df = pd.read_csv('data/ts_averages.csv', converters=conv)
df = pd.read_csv('data/ts_averages.csv')

df['BOLD [S.A.U.]'] = df['BOLD [S.A.U.]'].apply(ast.literal_eval)

# Filter entries for this selection
df = df.loc[df['Group']=='non-PMCA']
df = df.loc[df['Task']=='rhp10']

df['Anesthesia'] = ''
df.loc[df['Session'].str.contains('ketxyl'), 'Anesthesia'] = 'Ketamine-Xylazine'
df.loc[df['Session'].str.contains('isofl'), 'Anesthesia'] = 'Isoflurane'

df = df.explode('BOLD [S.A.U.]', ignore_index=True)
df['Time[s]'] = df.groupby(['ROI Path','Window']).cumcount()
df['BOLD [S.A.U.]'] = df['BOLD [S.A.U.]'].astype(float)

ax = sns.lineplot(data=df,
	x="Time[s]",
	y='BOLD [S.A.U.]',
	hue="Anesthesia",
)
#ax.set_yscale('logparticipant')
