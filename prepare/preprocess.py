from samri.pipelines.preprocess import generic

scratch_dir = '~/.scratch/nvcz'

bids_base = '{}/bids'.format(scratch_dir)

functional_scan_only_subjects=[
	'6756',
	'6757',
	'6758',
	'6759',
	'6760',
	'6761',
	'6935',
	'6936',
	'6937',
	'6938',
	'6939',
	'6941',
	'6942',
	]
all_scan_subjects=[
	'7293',
	'7294',
	'7295',
	'7296',
	'7297',
	'7487',
	'7491',
	'7504',
	'7505',
	'7507',
	'7004',
	'7005',
	'7006',
	'7008',
	'7009',
	'7010',
	'7011',
	]

generic(bids_base,
	'/usr/share/mouse-brain-atlases/dsurqec_200micron.nii',
	functional_blur_xy=0.25,
	registration_mask='/usr/share/mouse-brain-atlases/dsurqec_200micron_mask.nii',
        subjects=all_scan_subjects,
	exclude={'subject':functional_scan_only_subjects,},
	functional_match={'acquisition':['geEPI'],},
	structural_match={'acquisition':['TurboRARE'],},
	out_base=scratch_dir,
	workflow_name='preprocessed',
	enforce_dummy_scans=60,
	)

generic(bids_base,
	'/usr/share/mouse-brain-atlases/dsurqec_200micron.nii',
	functional_blur_xy=0.25,
	registration_mask='/usr/share/mouse-brain-atlases/dsurqec_200micron_mask.nii',
	subjects=functional_scan_only_subjects,
	functional_match={'acquisition':['geEPI'],},
	structural_match={'acquisition':['TurboRARE'],},
	out_base=scratch_dir,
	workflow_name='preprocessed',
	enforce_dummy_scans=60,
	functional_registration_method='functional',
	)
