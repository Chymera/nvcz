from os import path
from samri.plotting import summary
from samri.utilities import bids_substitution_iterator
from samri.pipelines.utils import bids_data_selection

my_roi = '../data/rhp.nii.gz'
data_dir = '~/.scratch/nvcz'
preprocessed_dir = path.expanduser(path.join(data_dir,'preprocessed'))

preprocessed_data = bids_data_selection(preprocessed_dir,
	structural_match=False,
	functional_match={'acquisition':['geEPI']},
	subjects=False,
	sessions=False,
	)
sessions = preprocessed_data['session'].unique()
runs = preprocessed_data['run'].unique()
tasks = preprocessed_data['task'].unique()
suffixes = preprocessed_data['suffix'].unique()
subjects = preprocessed_data['subject'].unique()

astrocytes_substitutions = bids_substitution_iterator(
	sessions=sessions,
	subjects=subjects,
	runs=runs,
	tasks=tasks,
	modalities=suffixes,
	data_dir='~/.scratch/nvcz',
	validate_for_template="{data_dir}/l1_astrocytes/sub-{subject}/ses-{session}/sub-{subject}_ses-{session}_task-{task}_acq-geEPI_run-{run}_{modality}.nii.gz",
	)

# The timecourses are ROI and not regressor-specific
timecourses, astrocytes_designs, _, events_dfs, subplot_titles = summary.ts_overviews(astrocytes_substitutions, my_roi,
	ts_file_template="{data_dir}/l1_astrocytes/sub-{subject}/ses-{session}/sub-{subject}_ses-{session}_task-{task}_acq-geEPI_run-{run}_{modality}.nii.gz",
	betas_file_template="{data_dir}/l1_astrocytes/sub-{subject}/ses-{session}/sub-{subject}_ses-{session}_task-{task}_acq-geEPI_run-{run}_desc-betas_{modality}.nii.gz",
	design_file_template="{data_dir}/l1_astrocytes/sub-{subject}/ses-{session}/sub-{subject}_ses-{session}_task-{task}_acq-geEPI_run-{run}_desc-design_{modality}.mat",
	event_file_template='{data_dir}/preprocessed/sub-{subject}/ses-{session}/func/sub-{subject}_ses-{session}_task-{task}_acq-geEPI_run-{run}_events.tsv',
	n_jobs_percentage=0.3,
	)

# Saves the BOLD timecourses to disk, in the following directory:
debug_dir = '/var/tmp/samri_debug/{}/nvcz'

import getpass
debug_dir = debug_dir.format(getpass.getuser())

import os
if not os.path.exists(debug_dir):
	    os.makedirs(debug_dir)

import numpy as np
for ix, i in enumerate(timecourses):
	nicer_filename = subplot_titles[ix].replace(' | ','_')
	nicer_filename = nicer_filename.replace('Subject ','sub-')
	nicer_filename = nicer_filename.replace('Session ','ses-')
	nicer_filename = nicer_filename.replace('Run ','run-')
	np.savetxt(path.join(debug_dir,'{}.csv').format(nicer_filename), i)
# End of BOLD saving sections
