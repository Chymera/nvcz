import itertools
import matplotlib.pyplot as plt
import multiprocessing as mp
from os import path
from joblib import Parallel, delayed
from samri.plotting.maps import contour_slices
from samri.pipelines.utils import bids_data_selection

num_cores = max(mp.cpu_count()-1,1)

scratch_dir=path.expanduser('~/.scratch/nvcz')
cmap = plt.get_cmap('tab20').colors
spacing = 0.5

def func_contour_slices(func_path, scratch_dir, subject, session, run, spacing):
	contour_slices(func_path,
		alpha=[0.9],
		colors=cmap[::2],
		figure_title='Single-Session Fit and Distortion Control\n Subject {} | Session {} | Run {}'.format(subject, session, run),
		file_template='/usr/share/mouse-brain-atlases/dsurqec_40micron_masked.nii',
		force_reverse_slice_order=True,
		levels_percentile=[79],
		ratio=[5,5],
		slice_spacing=spacing,
		save_as='{}/preprocess_qc/{}_{}_{}.pdf'.format(scratch_dir,subject,session,run),
		)
#def anat_contour_slices(substitution,file_path,data_dir,spacing):
#	contour_slices(file_path.format(**substitution),
#		alpha=[0.9],
#		colors=cmap[::2],
#		figure_title='Single-Session Fit and Distortion Control\n Subject {} | Session {} | Contrast T2'.format(i[0],substitution['session']),
#		file_template=templates[key],
#		force_reverse_slice_order=True,
#		levels_percentile=[79],
#		ratio=[5,5],
#		slice_spacing=spacing,
#		save_as='{}/manual_overview/{}/{}_{}_T2w.pdf'.format(data_dir,key,i[0],substitution['session']),
#		)

preprocessed_dir = path.join(scratch_dir, 'preprocessed/')
data_selection = bids_data_selection(preprocessed_dir, structural_match=False, functional_match={'acquisition':['geEPI']}, subjects=False, sessions=False)

func_paths = data_selection['path'].values.tolist()
subjects = data_selection['subject'].values.tolist()
sessions = data_selection['session'].values.tolist()
runs = data_selection['run'].values.tolist()

Parallel(n_jobs=num_cores,verbose=0)(map(delayed(func_contour_slices),
	func_paths,
	[scratch_dir]*len(func_paths),
	subjects,
	sessions,
	runs,
	[spacing]*len(func_paths),
	))

#anat_path='{{data_dir}}/preprocessing/{}_collapsed/sub-{{subject}}/ses-{{session}}/anat/sub-{{subject}}_ses-{{session}}_acq-TurboRARElowcov_T2w.nii.gz'.format(key,i[1],runs[i[1]])
#anat_substitutions = bids_substitution_iterator(
#	sessions=sessions,
#	subjects=[i[0]],
#	data_dir=data_dir,
#	validate_for_template=anat_path,
#	)
#Parallel(n_jobs=num_cores,verbose=0)(map(delayed(anat_contour_slices),
#	anat_substitutions,
#	[anat_path]*len(anat_substitutions),
#	[data_dir]*len(anat_substitutions),
#	[key]*len(anat_substitutions),
#	[i]*len(anat_substitutions),
#	[spacing]*len(anat_substitutions),
#	))
#
#contour_slices(templates[key],
#	alpha=[0.6],
#	colors=cmap[::2],
#	figure_title='Multi-Session Coherence Control\n Subject {} | Task {}'.format(i[0],runs[i[1]]),
#	file_template=func_path,
#	force_reverse_slice_order=True,
#	legend_template='{session} session',
#	levels_percentile=[77],
#	save_as='{}/preprocess_qc/{}/coherence_{}_{}.pdf'.format(data_dir,key,i[0],runs[i[1]]),
#	slice_spacing=spacing,
#	substitutions=func_substitutions,
#	)
