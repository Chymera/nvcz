import numpy as np
import pandas as pd
from samri.pipelines.utils import bids_data_selection
from samri.report.roi import ts
from os import path
from copy import deepcopy

# we define the directories
scratch_dir = path.abspath(path.expanduser('~/.scratch/nvcz'))
l1_dir = path.join(scratch_dir,'l1')

# we read the file where treatment group membership is recorded
participants_file = path.join(l1_dir,'participants.tsv')
participants = pd.read_csv(participants_file,sep='\t')

# we create lists for treatment group animals
pmca_animals = participants.loc[participants['group']=='PMCA', 'participant_id'].values.tolist()
non_pmca_animals = participants.loc[participants['group']!='PMCA', 'participant_id'].values.tolist()

pmca_animals = [str(i) for i in pmca_animals]
non_pmca_animals = [str(i) for i in non_pmca_animals]

# we select scans according to relevant selection criteria
data_selection = bids_data_selection(l1_dir,
	structural_match=False,
	functional_match={
		'suffix':['bold'],
		'acquisition':['geEPI'],
		},
	subjects=pmca_animals+non_pmca_animals,
	sessions=False,
	)
data_selection = data_selection.loc[~data_selection['path'].str.contains('_desc-')]
for index, row in data_selection.iterrows():
	ts_name = row['path']
	sub_str = '_bold.'
	ts_name_base = ts_name[:ts_name.index(sub_str)]

	#rhp
	ts_mean, ts_median = ts(row['path'], mask='../data/rhp.nii.gz')
	ts_name_mean = ts_name_base+'_desc-rhpMEAN.csv'
	ts_name_median = ts_name_base+'_desc-rhpMEDIAN.csv'

	np.savetxt(ts_name_mean, ts_mean, delimiter=',')
	np.savetxt(ts_name_median, ts_median, delimiter=',')

	#lhp
	ts_mean, ts_median = ts(row['path'], mask='../data/lhp.nii.gz')
	ts_name_mean = ts_name_base+'_desc-lhpMEAN.csv'
	ts_name_median = ts_name_base+'_desc-lhpMEDIAN.csv'

	np.savetxt(ts_name_mean, ts_mean, delimiter=',')
	np.savetxt(ts_name_median, ts_median, delimiter=',')

# we define the directories
scratch_dir = path.abspath(path.expanduser('~/.scratch/nvcz'))
l1_dir = path.join(scratch_dir,'l1')

# we read the file where treatment group membership is recorded
participants_file = path.join(l1_dir,'participants.tsv')
participants = pd.read_csv(participants_file,sep='\t')

# we create lists for treatment group animals
pmca_animals = participants.loc[participants['group']=='PMCA', 'participant_id'].values.tolist()
non_pmca_animals = participants.loc[participants['group']!='PMCA', 'participant_id'].values.tolist()

pmca_animals = [str(i) for i in pmca_animals]
non_pmca_animals = [str(i) for i in non_pmca_animals]

# we select scans according to relevant selection criteria
data_selection = bids_data_selection(l1_dir,
	structural_match=False,
	functional_match={
		'acquisition':['geEPI'],
		'session':[
			'isofl',
			'ketxyl',
			'ketxylr1',
			'ketxylr2',
			'ketxylr3',
			],
		'task':[
			'lhp05',
			'rhp05',
			'rhp10',
			],
		},
	subjects=non_pmca_animals+pmca_animals,
	sessions=False,
	)

# we compute the paths for the extracted ROI timecourses
data_selection = data_selection.loc[~data_selection['path'].str.contains('_desc-')]
data_selection = data_selection.loc[data_selection['path'].str.contains('_bold.nii.gz')]
data_selection['path_lhp'] = data_selection['path']
data_selection['path_lhp'] = data_selection['path_lhp'].str.replace('_bold.nii.gz','_desc-lhpMEDIAN.csv')
data_selection['path_rhp'] = data_selection['path']
data_selection['path_rhp'] = data_selection['path_rhp'].str.replace('_bold.nii.gz','_desc-rhpMEDIAN.csv')

#print(data_selection.shape[0], data_selection.columns.tolist())
keep_columns = data_selection.columns.tolist()
keep_columns.remove('path_lhp')
keep_columns.remove('path_rhp')
keep_columns.remove('suffix')
data_selection = pd.melt(data_selection, id_vars=keep_columns, value_vars=['path_lhp','path_rhp'], var_name='ROI path', ignore_index=True)
#print(data_selection.shape[0], data_selection.columns)
data_selection['ROI path'] = data_selection['value']
#print(data_selection['ROI path'].tolist())
data_selection = data_selection.drop(['value','scan_type','path'], axis=1)
data_selection['group'] = ''
data_selection.loc[data_selection['subject'].isin(pmca_animals), 'group'] = 'PMCA'
data_selection.loc[data_selection['subject'].isin(non_pmca_animals), 'group'] = 'non-PMCA'

# we define the event file pattern:
event_file_pattern = 'preprocessed/sub-{subject}/ses-{session}/func/sub-{subject}_ses-{session}_task-{task}_acq-{acquisition}_run-{run}_events.tsv'
event_file_pattern = path.join(scratch_dir,event_file_pattern)


# we define the range of the per-event averaging window in terms of seconds before and second after onser
window_start = -10
window_end = 60

new_rows = []
for index, row in data_selection.iterrows():
	event_file = event_file_pattern.format(**row)
	event_data = pd.read_csv(event_file, sep='\t')
	data_path = row['ROI path']
	my_data = genfromtxt(data_path, delimiter='\n')
	prev_window_end = 0
	row['section'] = []
	for event_index, event_row in event_data.iterrows():
		section_row = deepcopy(row)
		window_stim_onset = event_row['onset']
		if window_stim_onset - window_start < prev_window_end:
			raise ValueError('The time {}-th window of scan {} overlaps with the end of the previous window'.format(event_index, row['path']))
		this_window_start = int(window_stim_onset+window_start)
		this_window_end = int(window_stim_onset+window_end)
		segment = my_data[this_window_start:this_window_end]
		# apply baseline
		segment = segment - np.mean(my_data[this_window_start:window_stim_onset])
		segment = segment.tolist()
		section_row['data'] = segment
		section_row['section'] = event_index
		new_rows.append(section_row)
		prev_window_end = this_window_end
df = pd.DataFrame(new_rows)
df.to_csv('../data/ts_averages.csv')
