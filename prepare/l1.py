from os import path
from samri.pipelines import glm

scratch_dir = '~/.scratch/nvcz'

preprocess_base = path.join(scratch_dir,'preprocessed')

glm.l1(preprocess_base,
	highpass_sigma=180,
	lowpass_sigma=3,
        habituation=None,
	mask='mouse',
	n_jobs_percentage=.33,
	exclude={'task':['rest']},
	match={'suffix':['bold']},
	invert=False,
	workflow_name='l1',
	out_base=scratch_dir,
	keep_work=True,
	)

glm.l1_physio(preprocess_base,'astrocytes',
	highpass_sigma=180,
	lowpass_sigma=3,
	convolution=False,
	mask='mouse',
	n_jobs_percentage=.33,
#	exclude={'subject':lr_stim_subjects},
	match={'suffix':['bold']},
	invert=False,
	workflow_name='l1_astrocytes',
	out_base=scratch_dir,
	keep_work=True,
	)
glm.l1_physio(preprocess_base,'neurons',
	highpass_sigma=180,
	lowpass_sigma=3,
	mask='mouse',
	n_jobs_percentage=.33,
#	exclude={'subject':lr_stim_subjects},
	match={'suffix':['bold']},
	invert=False,
	workflow_name='l1_neurons',
	out_base=scratch_dir,
	keep_work=True,
	)

# Resting State Regressors
my_roi = '../data/rhp.nii.gz'

glm.seed(preprocess_base,my_roi,
        mask='mouse',
        n_jobs_percentage=.33,
        exclude={
		'session':['isofl'],
                },
        match={
                'task':['rest'],
                },
        lowpass_sigma=3,
        highpass_sigma=180,
        out_base=scratch_dir,
        workflow_name='l1_seed',
        metric='median',
        )
