import numpy as np
import pandas as pd
from samri.pipelines.utils import bids_data_selection
from os import path
from numpy import genfromtxt
from copy import deepcopy

# we define the directories
scratch_dir = path.abspath(path.expanduser('~/.scratch/nvcz'))
l1_dir = path.join(scratch_dir,'l1')

# we read the file where treatment group membership is recorded
participants_file = path.join(l1_dir,'participants.tsv')
participants = pd.read_csv(participants_file,sep='\t')

# we create lists for treatment group animals
pmca_animals = participants.loc[participants['group']=='PMCA', 'participant_id'].values.tolist()
non_pmca_animals = participants.loc[participants['group']!='PMCA', 'participant_id'].values.tolist()

pmca_animals = [str(i) for i in pmca_animals]
non_pmca_animals = [str(i) for i in non_pmca_animals]

# we select scans according to relevant selection criteria
data_selection = bids_data_selection(l1_dir,
	structural_match=False,
	functional_match={
		'acquisition':['geEPI'],
		'session':[
			'isofl',
			'ketxyl',
			'ketxylr1',
			'ketxylr2',
			'ketxylr3',
			],
		'task':[
			'lhp05',
			'rhp05',
			'rhp10',
			],
		},
	subjects=non_pmca_animals+pmca_animals,
	sessions=False,
	)

# we compute the paths for the extracted ROI timecourses
data_selection = data_selection.loc[~data_selection['path'].str.contains('_desc-')]
data_selection = data_selection.loc[data_selection['path'].str.contains('_bold.nii.gz')]
data_selection['path_lhp'] = data_selection['path']
data_selection['path_lhp'] = data_selection['path_lhp'].str.replace('_bold.nii.gz','_desc-lhpMEDIAN.csv')
data_selection['path_rhp'] = data_selection['path']
data_selection['path_rhp'] = data_selection['path_rhp'].str.replace('_bold.nii.gz','_desc-rhpMEDIAN.csv')

#print(data_selection.shape[0], data_selection.columns.tolist())
keep_columns = data_selection.columns.tolist()
keep_columns.remove('path_lhp')
keep_columns.remove('path_rhp')
keep_columns.remove('suffix')
data_selection = pd.melt(data_selection, id_vars=keep_columns, value_vars=['path_lhp','path_rhp'], var_name='ROI path', ignore_index=True)
#print(data_selection.shape[0], data_selection.columns)
data_selection['ROI path'] = data_selection['value']
#print(data_selection['ROI path'].tolist())
data_selection = data_selection.drop(['value','scan_type','path'], axis=1)
data_selection['group'] = ''
data_selection.loc[data_selection['subject'].isin(pmca_animals), 'group'] = 'PMCA'
data_selection.loc[data_selection['subject'].isin(non_pmca_animals), 'group'] = 'non-PMCA'

# we define the event file pattern:
event_file_pattern = 'preprocessed/sub-{subject}/ses-{session}/func/sub-{subject}_ses-{session}_task-{task}_acq-{acquisition}_run-{run}_events.tsv'
event_file_pattern = path.join(scratch_dir,event_file_pattern)


# we define the range of the per-event averaging window in terms of seconds before and second after onser
window_start = -15
window_end = 65

new_rows = []
for index, row in data_selection.iterrows():
	event_file = event_file_pattern.format(**row)
	event_data = pd.read_csv(event_file, sep='\t')
	data_path = row['ROI path']
	my_data = genfromtxt(data_path, delimiter='\n')
	prev_window_end = 0
	row['window'] = []
	# Standardize wrt. scan baseline
	baseline_end = int(event_data.loc[0]['onset'])
	mean = np.mean(my_data[:baseline_end])
	std = np.std(my_data[:baseline_end])
	my_data = (my_data-mean)/std
	for event_index, event_row in event_data.iterrows():
		window_row = deepcopy(row)
		stim_onset = int(event_row['onset'])
		if stim_onset - window_start < prev_window_end:
			raise ValueError('The time {}-th window of scan {} overlaps with the end of the previous window'.format(event_index, row['path']))
		this_window_start = int(stim_onset+window_start)
		this_window_end = int(stim_onset+window_end)
		segment = my_data[this_window_start:this_window_end]
		# baseline subtraction
		#segment = segment - np.mean(my_data[this_window_start:stim_onset])
		segment = segment.tolist()
		window_row['BOLD [s.A.U.]'] = segment
		window_row['window'] = event_index
		new_rows.append(window_row)
		prev_window_end = this_window_end
df = pd.DataFrame(new_rows)
df.rename(columns=lambda x: "".join([a if a.isupper() else b for a,b in zip(x,x.title())]), inplace=True)
#df.columns = df.columns.str.apply(lambda x: "".join([a if a.isupper() else b for a,b in zip(x,x.title())]))
#df.columns = df.columns.str.capitalize()
df.to_csv('../data/ts_averages.csv')
