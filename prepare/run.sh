#!/usr/bin/env bash

if [ ! -d ~/.scratch ]; then
	echo "You seem to be lacking a ~/.scratch/ directory."
	echo "We need this directory in order to process the data, and it needs to be on a volume with 200GB+ space."
	echo "You can simply symlink to a location you would like this to happen (and then re-run this script):
		ln -s /where/you/want/it ~/.scratch"
	exit 1
fi

#DATA_DIR="/mnt/data/nvcz"
DATA_DIR="/mnt/data/nvcz_data" 

if [ ! -d ~/.scratch/nvcz/bids ]; then
	if [ -d "/usr/share/nvcz_bidsdata" ]; then
		[ -d ~/.scratch/nvcz ] || mkdir -p ~/.scratch/nvcz
		ln -s /usr/share/nvcz_bidsdata ~/.scratch/nvcz/bids
	else
		echo "No NVCZ BIDS data distribution found, processing from NVCZ Bruker data distribution:"
		python make_bids.py || exit 1
	fi
fi

if [ ! -d ~/.scratch/nvcz/preprocessed ]; then
	python preprocess.py || exit 1
	# The following scans are unalignable with <=samri-0.7
	# sub-7292 ses-ketxyl run-0
	# sub-7295 ses-ketxyl run-1
	# sub-7293 ses-ketxyl run-1
	# sub-7009 ses-ketxylr1 run-0
	# sub-7297 ses-ketxyl run-1
	# sub-6760 ses-isofl run-0
fi
python preprocess_qc.py || exit 1
python l1.py || exit 1
python l1_save_bold_roi_ts.py || eixt 1
python l2.py || exit 1
sh transfer.sh || exit 1
